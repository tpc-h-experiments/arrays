\c tpch
create schema arrays_experiment;
set search_path to arrays_experiment;

create type arrays_experiment.lineitem as (
	l_partkey        integer,
	l_suppkey        integer,
	l_linenumber     integer,
	l_quantity       numeric(15,2),
	l_extendedprice  numeric(15,2),
	l_discount       numeric(15,2),
	l_tax            numeric(15,2),
	l_returnflag     character(1),
	l_linestatus     character(1),
	l_shipdate       date,
	l_commitdate     date,
	l_receiptdate    date,
	l_shipinstruct   character(25),
	l_shipmode       character(10),
	l_comment        character varying(44)
);

create type arrays_experiment.nested_order as (
	o_orderkey       integer,
	o_orderstatus    character(1),
	o_totalprice     numeric(15,2),
	o_orderdate      date,
	o_orderpriority  character(15),
	o_clerk          character(15),
	o_shippriority   integer,
	o_comment        character varying(79),
	o_lineitems	 lineitem[]
);

create type arrays_experiment.customer as (
	c_custkey     integer,
	c_name        character varying(25),
	c_address     character varying(40),
	c_nationkey   integer,
	c_phone       character(15),
	c_acctbal     numeric(15,2),
	c_mktsegment  character(10),
	c_comment     character varying(117),
	c_orders      nested_order[]
);

create table customers as
select
	c.*,
	coalesce(array_agg(
		ROW(o_orderkey,o_orderstatus,o_totalprice,o_orderdate,o_orderpriority,
		o_clerk,o_shippriority,o_comment,o_lineitems)::nested_order
	) filter (where o_orderkey is not null), '{}') as c_orders
from
	scale1.customer c left outer join (
		select
			o.*,
			coalesce(array_agg(
				ROW(l_partkey,l_suppkey,l_linenumber,l_quantity,l_extendedprice,l_discount,l_tax,
				l_returnflag,l_linestatus,l_shipdate,l_commitdate,l_receiptdate,l_shipinstruct,l_shipmode,
				l_comment)::lineitem
			) filter (where l_linenumber is not null), '{}') as o_lineitems
		from
			scale1.orders o left outer join scale1.lineitem l
			on o.o_orderkey = l.l_orderkey
		group by
			o.o_orderkey,
			o_orderstatus,
			o_totalprice,
			o_orderdate,
			o_orderpriority,
			o_clerk,
			o_shippriority,
			o_comment,
			o.o_custkey
	) ol on c.c_custkey = ol.o_custkey
group by
	c_custkey,
	c_name,
	c_address,
	c_nationkey,
	c_phone,
	c_acctbal,
	c_mktsegment,
	c_comment;
