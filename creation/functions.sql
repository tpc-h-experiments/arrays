\c tpch
set search_path to arrays_experiment;

create or replace function q1filter(c customers) returns table (l_returnflag character(1), l_linestatus character(1), l_quantity numeric(15,2), l_extendedprice numeric(15,2), l_disc_price numeric(15,2), l_charge numeric(15,2), l_discount numeric(15,2)) as
$body$
declare
	o nested_order;
	l lineitem;
begin
foreach o in array c.c_orders
loop
	foreach l in array o.o_lineitems
	loop
		if l.l_shipdate <= date '1992-04-30' - interval '119 days'
		then
			l_returnflag := l.l_returnflag;
			l_linestatus := l.l_linestatus;
			l_quantity := l.l_quantity;
			l_extendedprice := l.l_extendedprice;
			l_disc_price := l.l_extendedprice * (1 - l.l_discount);
			l_charge := l_disc_price * (1 + l.l_tax);
			l_discount := l.l_discount;
			return next;
		end if;
	end loop;
end loop;
end;
$body$
language 'plpgsql';

create or replace function q3filter(o nested_order) returns table (l_discount numeric(15,2), l_extendedprice numeric(15,2)) as
$body$
declare
	l lineitem;
begin
foreach l in array o.o_lineitems
loop
	if l.l_shipdate > date '1992-01-02'
	then
		l_discount := l.l_discount;
		l_extendedprice := l.l_extendedprice;
		return next;
	end if;
end loop;
end;
$body$
language 'plpgsql';
			
create or replace function q4filter(c customers) returns setof nested_order as
$body$
declare
	o nested_order;
	l lineitem;
	f boolean := false;
begin
foreach o in array c.c_orders
loop
	if o.o_orderdate >= date '1992-01-01' and o.o_orderdate < date '1992-01-01' + interval '3' month
	then
		foreach l in array o.o_lineitems
		loop
			if l.l_commitdate < l.l_receiptdate
			then
				f := true
				exit;
			end if;
		end loop;
		if f then
		   return next o;
		   f := false;
		end if;
	end if;
end loop;
return;
end;
$body$
language 'plpgsql';

create or replace function q12filter(o nested_order) returns setof lineitem as
$body$
declare
	l lineitem;
begin
foreach l in array o.o_lineitems
loop
	if l.l_shipmode in ('RAIL', 'REG AIR') 
	and l.l_commitdate < l.l_receiptdate
	and l.l_shipdate < l.l_commitdate
	and l.l_receiptdate >= date '1992-01-01'
	and l.l_receiptdate < date '1992-01-01' + interval '1' year
	then
		return next l;
	end if;
end loop;
return;
end;
$body$
language 'plpgsql';
