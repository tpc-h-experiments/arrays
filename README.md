# Setup environment

 - Install PostgreSQL (v10 was used at the moment this was wrote)
 - Load TPC-H dataset in PostgreSQL
 - Run `psql -f creation/creation.sql`. This will create a new schema called `array_experiments` with the necessary
   user defined types (UDTs) and table `customers`, with Orders nested in customers and lineitems nested in
   orders
 - Run `psql -f creation/functions.sql`. This will create the functions used to retrieve elements
   from arrays based on a certain condition, but without unnesting

# Runtime data collection

Run from bash:
	```
	explain=`psql -f file.sql`
	runtime=`awk '/Execution time/{sum += $3; n++} END {print sum/n}' <<< $explain`
	```
Alternatively, you can store the explain within a file and then filter it using awk

## Analysis

 - Extra projections make runtime slightly slower than manually pushing down selections
 - **Projection:** It seems that the system has some trouble projecting attributes when using the 'unnest' first approach.
 - **Selection order:** In the relational side, the order in which selections are done depends on the order in 
   which joins are done. In the 'document' side, they depend more on the structure of the document. 
   In particular, we can force selections to go 'top to bottom' in the structure of the data. 
   This seems to have a positive effect in performance (at least, no harm).
 - **Functions**: the function overhead kills any advantages that the direct access could have.
   The goal for the functions was to have a step where we would retrieve only the elements of an array that 
   would pass a condition in them to the next step. Using unnest, this may or may not be 
   accomplished (see previous step). 
 - Writing the queries over arrays and nesting subqueries in FROM to make sure that objects are filtered as soon 
   as possible and that projections are done as soon as possible leads to very competitive results. It is still 
   the case that when the query runs over a single table only (e.g. Q1) relational is better, as it reads 
   less data. In a sense, the relational model acts as columnar data, in that it is more restricted in the data 
   that is read.
 - Q3 and Q4 were better than relational, whereas Q12 and Q22 were very similar to it on small scales. This
   approach does not seem to scale very well as the difference in speed, compared to relational,
   increases as the dataset grows in size.

## References and useful links

 - https://dzone.com/articles/nested-data-structures-and-non
