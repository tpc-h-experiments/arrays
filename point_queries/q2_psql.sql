\c tpch
set search_path to arrays_experiment;
explain analyze select l_returnflag 
from (select (unnest(o_lineitems)::lineitem).* from (select (unnest(c_orders)::nested_order).* from customers where c_mktsegment = 'AUTOMOBILE') o where o_orderdate >= date '1992-01-01') l where l_returnflag = 'R';
