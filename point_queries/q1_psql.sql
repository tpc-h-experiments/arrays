\c tpch
set search_path to arrays_experiment;

-- Extra projections make runtime slightly slower than manually pushing down selections
explain analyze select l_returnflag 
from (select o_orderdate, c_mktsegment, (unnest(o_lineitems)::lineitem).* from (select c_mktsegment, (unnest(c_orders)::nested_order).* from customers c) o) l where c_mktsegment = 'AUTOMOBILE' and o_orderdate >= date '1992-01-01' and l_returnflag = 'R';
