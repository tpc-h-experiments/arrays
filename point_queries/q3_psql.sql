\c tpch
set search_path to scale1;
explain analyze select l_returnflag from customer c, orders o, lineitem l where c.c_custkey = o.o_custkey and o.o_orderkey = l.l_orderkey and c_mktsegment = 'AUTOMOBILE' and o_orderdate >= date '1992-01-01' and l_returnflag = 'R';
