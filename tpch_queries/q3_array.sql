\c tpch
set search_path to arrays_experiment;

explain analyze select 
	o_orderkey as l_orderkey,
	sum(l_extendedprice * (1 - l_discount)) as revenue,
	o_orderdate,
	o_shippriority
from (
	select
		o_orderkey, o_orderdate, o_shippriority, (unnest(o_lineitems)::lineitem).*
	from (
		select
			(unnest(c_orders)::nested_order).*
		from
			customers
		where
			c_mktsegment = 'AUTOMOBILE'
	) o
	where
		o_orderdate < date '1992-01-02'
) ol
where
	l_shipdate > date '1992-01-02'
group by
      l_orderkey,
      o_orderdate,
      o_shippriority
order by
      revenue desc,
      o_orderdate
limit 10;
