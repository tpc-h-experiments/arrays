\c tpch
set search_path to arrays_experiment;
explain analyze select
	l.l_returnflag,
	l.l_linestatus,
	sum(l.l_quantity) as sum_qty,
	sum(l.l_extendedprice) as sum_base_price,
	sum(l.l_disc_price) as sum_disc_price,
	sum(l.l_charge) as sum_charge,
	avg(l.l_quantity) as avg_qty,
	avg(l.l_extendedprice) as avg_price,
	avg(l.l_discount) as avg_disc,
	count(*) as count_order
from
	(select (q1filter(c)).* from customers c) l
group by
	l.l_returnflag,
	l.l_linestatus
order by
	l.l_returnflag,
	l.l_linestatus;
