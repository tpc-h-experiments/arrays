\c tpch
set search_path to arrays_experiment;

explain analyze select
	c_count,
	count(*) as custdist
from
	(
		select
			c_custkey,
			count(o_orderkey) filter (where o_comment not like '%express%packages%')
		from (
			select
				c_custkey,
				(unnest(
					case when c_orders <> '{}' then
					     c_orders
					else 
					     '{null}' 
					end
				)::nested_order).*
			from
				customers
		) co
		group by
			c_custkey
	) as c_orders (c_custkey, c_count)
group by
	c_count
order by
	custdist desc,
	c_count desc;
