\c tpch
set search_path to arrays_experiment;

explain analyze select
        l_shipmode,
        sum(case
                when o_orderpriority = '1-URGENT'
                        or o_orderpriority = '2-HIGH'
                        then 1
                else 0
        end) as high_line_count,
        sum(case
                when o_orderpriority <> '1-URGENT'
                        and o_orderpriority <> '2-HIGH'
                        then 1
                else 0
        end) as low_line_count
from (
	select
		o_orderpriority, (q12filter(o)).*
	from (
		select
			(unnest(c_orders)::nested_order).*
		from
			customers
	) o
) ol
group by
      l_shipmode
order by
      l_shipmode;
