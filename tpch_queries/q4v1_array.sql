\c tpch
set search_path to arrays_experiment;

explain analyze select
	o_orderpriority,
	count(*) as order_count
from
	(select	(unnest(c_orders)::nested_order).* from customers) o
where
	o_orderdate >= date '1992-01-01'
	and o_orderdate < date '1992-01-01' + interval '3' month
	and exists (
		select
			*
		from
			(select	(unnest(o.o_lineitems)::lineitem).*) l
		where
			l.l_commitdate < l.l_receiptdate
	)
group by
	o_orderpriority
order by
	o_orderpriority;
