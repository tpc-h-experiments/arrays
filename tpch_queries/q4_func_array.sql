\c tpch
set search_path to arrays_experiment;

explain analyze select
	o_orderpriority,
	count(*) as order_count
from 
     (select (q4filter(c)).* from customers c) o
group by
	o_orderpriority
order by
	o_orderpriority;
